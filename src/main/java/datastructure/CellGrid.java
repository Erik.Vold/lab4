package datastructure;

import cellular.CellState;
import java.util.ArrayList;


public class CellGrid implements IGrid {
	private int  rows ;
	private int  cols ; 
	private int length ;
	ArrayList<CellState> grid;
	ArrayList<CellState> gridCopy;
	
	CellState initialState ;
	CellState initNewState = CellState.DEAD;

    public CellGrid(int rows, int columns, CellState initialState) {
		if ( rows <= 0 || columns <= 0 )
			throw new IndexOutOfBoundsException("Value rows can not be null or negative");
		this.rows = rows;
		this.cols = columns;
		this.initialState = initialState;
		this.length = this.rows*this.cols ;
		grid = new ArrayList<CellState>();

	for (int i = 0; i < this.rows ; i++) {
		for (int j = 0; j < this.cols; j++){
			grid.add(j + i*this.cols, this.initialState);

			} 
		}
	}


    @Override
    public int numRows() {
  
        return this.rows;
    }

    @Override
    public int numColumns() {
     
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
		if (  row > this.rows || column > this.cols )
			throw new IndexOutOfBoundsException("Value for row can not be null or negative");
		if ( row < 0 || column < 0 )
			throw new IndexOutOfBoundsException("Value for row can not be null or negative");

		int index = column + row*this.cols ;
		if (index >= this.length ) {
				throw new IndexOutOfBoundsException("Value can not be outside array");
		}
		this.grid.set(index, element);
		return;
		
        
    }

    @Override
    public CellState get(int row, int column) {
		if (  row > this.numRows() || column > this.numColumns() ) {
			throw new IndexOutOfBoundsException("Value for row can not be null or negative");
		} else if ( row < 0 || column < 0 ) {
			throw new IndexOutOfBoundsException("Value for row can not be null or negative");
		} else if ((column + row*this.numColumns()) >= (this.numRows()*this.numColumns() )) { 
			throw new IndexOutOfBoundsException("Index can not be outside array") ;
		} else {
		int index = column + row*this.numColumns() ;
        return this.grid.get(index ); 
		}
    }

    @Override
    public IGrid copy() {
		IGrid newGrid = new CellGrid(this.rows, this.cols,  initNewState);

		gridCopy = new ArrayList<CellState>();
		CellState element ;
		for (int i = 0; i < this.rows ; i++) {
			for (int j = 0; j < numColumns() ; j++){
				int index = j + i*numColumns();
				element = this.grid.get(index);
			
				newGrid.set(i, j, element);
	
				} 
			}
		
        return newGrid;
    }
    
}
